﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kool
{
    class Program
    {
        static void Main(string[] args)
        {
            string õpilasteNimekiri = @"..\..\õpilased.txt";
            var loetudõpilased = File.ReadAllLines(õpilasteNimekiri);
            foreach (var x in loetudõpilased)
            {
                var jupid = x.Split(',');
                Õpilane uusÕpilane = Õpilane.New(jupid[0].TrimStart(' '));
                uusÕpilane.KlassKusÕpib = jupid[3].TrimStart(' ');
                uusÕpilane.EesNimi = jupid[1].TrimStart(' ');
                uusÕpilane.PereNimi = jupid[2].TrimStart(' ');
            }

            string õpetajateNimekiri = @"..\..\õpetajad.txt";
            var loetudõpetajad = File.ReadAllLines(õpetajateNimekiri);
            foreach (var x in loetudõpetajad)
            {
                var jupid = x.Split(',');
                if (jupid.Length == 5)
                {
                    Õpetaja uusÕpetaja = Õpetaja.New(jupid[0].TrimStart(' '));
                    uusÕpetaja.AineMidaÕpetab = jupid[3].TrimStart(' ');
                    uusÕpetaja.KlassMidaJuhatab = jupid[4].TrimStart(' ');
                    uusÕpetaja.EesNimi = jupid[1].TrimStart(' ');
                    uusÕpetaja.PereNimi = jupid[2].TrimStart(' ');
                } else
                {
                    Õpetaja uusÕpetaja = Õpetaja.New(jupid[0].TrimStart(' '));
                    uusÕpetaja.AineMidaÕpetab = jupid[3].TrimStart(' ');
                    uusÕpetaja.KlassMidaJuhatab = "puudub";
                    uusÕpetaja.EesNimi = jupid[1].TrimStart(' ');
                    uusÕpetaja.PereNimi = jupid[2].TrimStart(' ');
                }

            }
          
            foreach (var x in Inimene.Kool.Values)
                Console.WriteLine( x.ToString() ); 

            Console.WriteLine("Sünnipäevade loetelu:");
            foreach (var x in Inimene.Kool.Values) //Values siis võtab kõik Valued
                Console.WriteLine($"{x.Nimi} on sündinud {x.sünnikuupäev:dd. MMMM yyyy.} ja ta on {x.Age} aastat vana"); //siis trükib välja ilma kellaajata või: $"{xValue.Birthday:dd.MM.yyyy}" x.sünnikuupäev.ToString("dd. MMMM yyyy. a")

            //kellel on järgmisena sünnipäev:
            DateTime next = DateTime.Today.AddDays(400); //maksimumväärtus
            string kes = "";
            Console.WriteLine(next);
            foreach (var x in Inimene.Kool)
            {
                if (x.Value.sünnikuupäev.AddYears(x.Value.Age +1) < next) // kas sünnipäev tuleb varem
                {
                    kes = x.Key;
                    next = x.Value.sünnikuupäev.AddYears(x.Value.Age + 1);
                }
            }
            Console.WriteLine($"järgmisena peab sünnipäeva {Inimene.Kool[kes].Nimi}");
            // saab ka:
            var ahaa = Inimene.Kool.Values.OrderBy(x => x.sünnikuupäev.AddYears(x.Age + 1)).FirstOrDefault();
            Console.WriteLine($"järgmine sünnipäevalaps on {ahaa}");
        }
    }

    class Inimene
    {
        public readonly string IK;
        public string EesNimi;
        public string PereNimi;
        public DateTime sünnikuupäev;
        public string Nimi => EesNimi + " " + PereNimi; // ei tohi väärtustada kontruktoris. siin defineeritakse
        public static Dictionary<string, Inimene> Kool = new Dictionary<string, Inimene>();

        // prodected kontructor siis saab ka tuletatud klassidest sisse
        // private kontructor siis saab ainult klassist sisse

        protected Inimene(string ik)
        {
            IK = ik;
            sünnikuupäev = Sünnikuupäev(ik);
            Kool.Add(IK, this);
        }

        protected static bool Kontrolli(string ik)
        {
            if (ik.Length == 11 && !Kool.ContainsKey(ik)) return true;
            else return false;
        }

        public static Inimene New(string ik)
        {
            bool kontrollitud = false;
            kontrollitud = Kontrolli(ik);
            if (kontrollitud) return new Inimene(ik);
            else return null;
        }
        public override string ToString()
        {
            return $"{Nimi} ({IK})";
        }

        static DateTime Sünnikuupäev(string ik)
        {
            return new DateTime(NormaalseInimeseAasta(ik), int.Parse(ik.Substring(3, 2)), int.Parse(ik.Substring(5, 2)));
        }

        static int NormaalseInimeseAasta(string ik)
        {
            int aastaosa = int.Parse(ik.Substring(1, 2));  //saab ka:
            switch (ik.Substring(0, 1))                       // int sajand = 1800:
            {                                                   // if(IK[0] > '2') sajand = 1900;
                case "1": case "2": return aastaosa + 1800;     // if(IK[0] > '4') sajand = 2000;    
                case "3": case "4": return aastaosa + 1900;
                case "5": case "6": return aastaosa + 2000;
            }
            throw new Exception("Isikukoodi esimene märk on vigane!");
        }
        public int Age => (DateTime.Today - sünnikuupäev).Days * 4 / 1461;
    }

    class Õpilane : Inimene
    {
        public string KlassKusÕpib;
        public static List<Õpilane> KõikÕpilased = new List<Õpilane>();

        protected Õpilane(string ik) : base(ik)
        {
             KõikÕpilased.Add(this);
        }

        public static new Õpilane New(string ik)
        {
            if (Kontrolli(ik)) return new Õpilane(ik);
            else return null;
        }

        public override string ToString()
        {
            return $"{KlassKusÕpib} klassi õpilane {Nimi}";
        }
    }

    class Õpetaja : Inimene
    {
        public string AineMidaÕpetab;
        public string KlassMidaJuhatab;
        public static List<Õpetaja> KõikÕpetajad = new List<Õpetaja>();

        protected Õpetaja(string ik) : base(ik)
        {

            KõikÕpetajad.Add(this);
        }

        public static new Õpetaja New(string ik)
        {
            if (Kontrolli(ik)) return new Õpetaja(ik);
            else return null;
        }

        public override string ToString()
        {
            return $"{AineMidaÕpetab} õpetaja {Nimi} ({KlassMidaJuhatab} klassi juhataja)";
        }
    }
}
